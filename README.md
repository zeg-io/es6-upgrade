# es6-update
This is a quick _(and dirty)_ way to clean up most migration issues from es5 code to es6+ code.

WARNING: The dirty... This will not clean up all feasable combinations of useless returns.  It may leave hanging chads in the form of not deleting closing parens. 

## Install
```js
npm i -g es6-upgrade
```

## Usage
```sh
es6-upgrade <filename> # or es6up
es6up --help

# To preview output
es6up -p <filename>
```
