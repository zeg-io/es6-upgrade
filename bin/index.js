#!/usr/bin/env node
const chalk = require('chalk')
const fs = require('fs')
const highlight = require('cli-highlight').highlight
const regExFunctions = require('../lib/regex')
const options = require('../lib/yargs-setup')
const { hr, showHeader, showRules } = require('../lib/messages')
const { log, logNoDecoration } = require('../lib/log')

const main = async () => {
  const filePath = options._.shift()

  if (options.help || options.h || options['?']) {
    return console.info('NO HELP')
  }

  if (options.rules) {
    return showRules()
  }
  if (
    (options.output && typeof options.output !== 'string') ||
    (options.output && typeof options.output === 'string' && !filePath)
  ) {
    return log(
      chalk.red(
        '--output requires an output location AND a source. Usage: es6-upgrade --help'
      )
    )
  }
  if (!filePath) {
    return log(
      chalk.red('No source file-path was passed in.  Usage: es6-upgrade --help')
    )
  }

  //showHeader()

  // EXECUTE A KNOWN GOOD COMMAND
  try {
    const startTime = new Date()
    const result = await regExFunctions(filePath)

    if (result) {
      if (options.o) {
        fs.writeFileSync(options.o, result)
      } else if (options.p) {
        log('CONVERSION OUTPUT:')
        logNoDecoration(
          highlight(result, { language: 'javascript', ignoreIllegals: true })
        )
      } else {
        fs.writeFileSync(filePath, result)
      }
    }
  } catch {
    //logNoDecoration(help(options._[0]))
  }
}

main().catch(err => console.error(err))
