// TODO" THIS IS BROKEN, FIX IT

const rx = /var ((.*)(\s?=\s?(.*))?,\n\s+)*(.*)(\s?=\s?(.*)\n)?/gm
// DESC LIMIT       |                                                  |
const description = 'DISABLED: var x,y to var x; var y;'
const rule = fileString => {
  let workingFile = fileString
  const wrappedVarMatches = fileString.match(rx)

  wrappedVarMatches.forEach(varChain => {
    const varSplit = varChain.split(',\n')

    if (varSplit.length > 1) {
      let iVar
      for (iVar = 0; iVar < varSplit.length; iVar++) {
        if (iVar === 0) {
          workingFile = workingFile.replace(
            `${varSplit[iVar]},`,
            varSplit[iVar]
          )
        } else {
          workingFile = workingFile.replace(
            RegExp(`${varSplit[iVar].trim()},?`, 'gm'),
            `var ${varSplit[iVar]}`
          )
        }
      }
    }
  })

  return workingFile
}
module.exports = {
  description,
  rule
}
