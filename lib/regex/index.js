/*
  newRequest
     .query\([\s\S]*?\)
*/
const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const replaceEngine = require('./replace-engine')

const convert = filePath => {
  if (!filePath.includes('.js') && !filePath.includes('.jsx'))
    return chalk.yellow(`[${filePath}] is not a JavaScript file.`)
  // write file in place
  if (fs.existsSync(filePath)) {
    const jsFile = fs.readFileSync(filePath, 'utf8')

    // const outputFile = replaceEngine(jsFile).anonymousFunctions

    return replaceEngine(jsFile)

    //process.stdout.write(outputFile)
  } else {
    console.error("File doesn't exist")
  }
}

module.exports = convert
