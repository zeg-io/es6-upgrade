const fs = require('fs')
const path = require('path')

const replaceEngine = fileString => {
  let newString = fileString

  const modules = fs.readdirSync(path.join(__dirname, './modules'))

  modules.forEach(module => {
    newString = require(`./modules/${module}`).rule(newString)
  })
  return newString
}

module.exports = replaceEngine
