const rx = [
  /=> {\s+(?:(?:\s+\/\/.*)?\s+)+?return(\s+)?((.*\s+)?(?:\/\/.*\n)|(.*))?\s+}/gm,
  (m1, garbage, returnedValue, returnedValueNoComment, X, y, z) => {
    let returnThis = returnedValue.trim()

    if (returnedValueNoComment) returnThis = returnedValueNoComment.trim()

    const lastChar = returnThis.substr(returnThis.length -1, 1)
    if (lastChar === ';') {
      returnThis = returnThis.substr(0, returnThis.length - 1)
    }
    return `=> ${returnThis}`
  }
]
// DESC LIMIT       |                                                  |
const description = 'If a func immediately returns, just use =>'
const rule = fileString => {
  return fileString.replace(...rx)
}
module.exports = {
  description,
  rule
}
