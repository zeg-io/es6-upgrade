const rx = [/function (\w+)\s?\((.*)\)\s?{/g, 'const $1 = ($2) => {']
// DESC LIMIT       |                                                  |
const description = 'Convert named func into constant => func'
const rule = fileString => {
  return fileString.replace(...rx)
}
module.exports = {
  description,
  rule
}
