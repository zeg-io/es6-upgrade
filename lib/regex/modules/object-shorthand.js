const rx = /(?:= {\s+)((.|\n)*?)(?:}\n)/gm
// DESC LIMIT       |                                                  |
const description = 'Convert { foo: foo, bar: bar } into { foo, bar }'
const rule = fileString => {
  const propArrays = fileString.match(rx)
  let workingFile = fileString

  if (propArrays) {
    propArrays
      .map(props => props.replace(/(=|{|})/g, '').split(','))
      .forEach(propArr => {
        propArr.forEach(propVal => {
          const prop = propVal.split(':')

          if (prop.length > 1 && prop[0].trim() === prop[1].trim()) {
            workingFile = workingFile.replace(
              RegExp(propVal.trim(), 'g'),
              prop[0].trim()
            )
          }
        })
      })
  }
  return workingFile
}
module.exports = {
  description,
  rule
}
