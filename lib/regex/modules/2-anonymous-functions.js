const rx = [/function?\s(.*)?\s{/g, '$1 => {']
// DESC LIMIT       |                                                  |
const description = 'Convert anonymous functions into arrow functions'
const rule = fileString => {
  return fileString.replace(...rx)
}
module.exports = {
  description,
  rule
}
