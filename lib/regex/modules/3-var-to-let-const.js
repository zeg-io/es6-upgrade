// DESC LIMIT       |                                                  |
const description = 'Dynamically convert vars into let or const'
const rule = fileString => {
  const varMatches = fileString.match(/var (\w+)/g)
  let workingFile = fileString

  const uniqueVars = [...new Set(varMatches)]
  uniqueVars
    .map(varWithDefinition => varWithDefinition.substr(4))
    .forEach(definedVariable => {
      // ^\s+((?!var )S4?\s=)
      const testResetVar = RegExp(
        `(^(\\s+)((?!var )${definedVariable}?\\s=)|${definedVariable}\\+\\+)`,
        'gm'
      )

      // const testResetVar = /^(\s+)((?!var )changes?\s=)/gm
      const testIsFunction = RegExp(`${definedVariable}?\\s\\(?\\s`, 'gm')

      if (testResetVar.test(workingFile)) {
        // var is reset, use let
        // console.info(definedVariable, 'found, use let')
        workingFile = workingFile.replace(
          RegExp(`var ${definedVariable}`, 'g'),
          `let ${definedVariable}`
        )
      } else if (testIsFunction.test(workingFile)) {
        // console.info(definedVariable, 'found is function, use const')
        workingFile = workingFile.replace(
          RegExp(`var ${definedVariable}`, 'g'),
          `const ${definedVariable}`
        )
      } else {
        // console.info(definedVariable, 'not reassigned, use const')
        workingFile = workingFile.replace(
          RegExp(`var ${definedVariable}`, 'g'),
          `const ${definedVariable}`
        )
      }
    })

  return workingFile
}
module.exports = {
  description,
  rule
}
