const rx = [/['|"]use strict['|"]\n/gm, '']
// DESC LIMIT       |                                                  |
const description = 'Remove "use strict"'
const rule = fileString => {
  return fileString.replace(...rx)
}
module.exports = {
  description,
  rule
}
