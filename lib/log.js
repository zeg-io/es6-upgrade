module.exports.log = message => {
  this.logNoDecoration(`es6up: ${message}`)
}
module.exports.logNoDecoration = message => {
  process.stdout.write(`${message}\n`)
}
