const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const { log, logNoDecoration } = require('./log')

const hr = () => {
  logNoDecoration(chalk.cyan('+'.padEnd(73, '-') + '+'))
}
const showHeader = () => {
  logNoDecoration(
    chalk.cyanBright("ES6 Upgrade: Let's get rid of that annoying es5 code!")
  )
  hr()
  logNoDecoration(
    chalk.cyan(
      `| Be aware that code that relies on ${chalk.green(
        'this'
      )} may break due to scope changes. |`
    )
  )
  hr()
}

const showRules = () => {
  let rulesOut = ''
  showHeader()
  const regexRules = fs.readdirSync(
    path.join(__dirname, '../lib/regex/modules')
  )

  regexRules.forEach(rule => {
    rulesOut +=
      `| ${rule.substr(0, rule.length - 3)}`.padEnd(23, ' ') +
      require(`../lib/regex/modules/${rule}`).description.padEnd(50, ' ') +
      '|\n'
  })
  logNoDecoration(chalk.cyan(rulesOut.substr(0, rulesOut.length - 1)))
  hr()
}

module.exports = {
  hr,
  showHeader,
  showRules
}
