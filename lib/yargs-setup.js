const yargs = require('yargs')

module.exports = yargs
  .usage('Usage: es6upgrade [options] <source file-path>')
  .command(
    '<source file-path>',
    'ES6 Upgrade upgrades particular aspects of your ES5 code to ES6+ format in place, overwriting the original file.'
  )
  .boolean(['p', 'r'])
  .option('o', {
    alias: 'output',
    describe: 'Output to a separate file rather than overwriting'
  })
  .option('p', { alias: 'preview', describe: 'Preview output to standard out' })
  .option('r', { alias: 'rules', describe: 'List supported rules' })
  .option('v', {
    alias: 'version',
    describe: 'gets the version'
  }).argv
